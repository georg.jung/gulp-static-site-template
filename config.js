var config = require('./config.defaults');

// set the url of your website here
config.location = 'https://example.com';

// enable this if you have a new version of git installed; use correct ISO date format for sitemap
// config.git_log_iso_date_formatstring = '%cI';

/*
// possibly add proxies to backend servers that provide dynamic content while debugging
config.proxies = [
  {source: '/forms', target: 'http://localhost:8001/forms', options: {}}
];
*/

/*
// possibly add individual per-page configuration for sitemap generation 
config.sitemap.mappings =  [
  {
    pages: [ 'index.html' ],
    priority: 1.0
  },
  {
    pages: [ 'impressum.html' ],
    priority: 0.1
  }
];
*/

/*
// possibly exlude i.e. the google site verification file from the sitemap
config.paths.src.sitemap = () => [config.paths.dest.html() + '*.html', '!public/google*.html'];
*/

module.exports = config;
